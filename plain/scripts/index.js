function randomizeListOrder(list) {
    if (!list || !list.children) return;
    for (var i = list.children.length; i >= 0; i--) {
        list.appendChild(list.children[Math.random() * i | 0]);
    }
}

// randomize all word clouds, call on load
(function() {
    var lists = document.getElementsByClassName('cloud');
    for (var i = 0; i <= lists.length; i++) {
        randomizeListOrder(lists[i]);
    }
})();

function toggleCollapse(button) {
    if (!button) return;
    button.classList.toggle('active');
    button.nextElementSibling.classList.toggle('collapsed');
}
