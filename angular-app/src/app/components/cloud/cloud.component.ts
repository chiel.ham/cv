import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

import { CloudItem } from './cloud.model';

@Component({
  selector: 'cloud, word-cloud',
  templateUrl: './cloud.component.html',
  styleUrls: ['./cloud.component.scss'],
  standalone: true,
  imports: [CommonModule]
})
export class CloudComponent {

  @Input() items?: CloudItem[];
}
