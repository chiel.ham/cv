export interface CloudItem {
  label: string;
  weight: number;
}
