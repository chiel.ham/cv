import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { BehaviorSubject, combineLatest, map, tap } from 'rxjs';

import { SkillService } from './skills.service';
import { LoaderComponent } from '../loader/loader.component';
import { CloudComponent } from '../cloud/cloud.component';
import { shuffleArray } from 'src/app/util/shuffle-array.function';

@Component({
  selector: '[skills]',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss'],
  standalone: true,
  imports: [CommonModule, LoaderComponent, CloudComponent],
  providers: [SkillService]
})
export class SkillsComponent {

  private readonly randomize$ = new BehaviorSubject<void>(undefined);

  readonly skills$ = combineLatest([
    this.service.getSkills(),
    this.randomize$]
  ).pipe(
    map(([skills, _]) => skills),
    tap(skills => shuffleArray(skills))
  );

  constructor(private readonly service: SkillService) {}

  randomizeOrder() {
    this.randomize$.next();
  }
}
