import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCakeCandles, faEnvelope, faLocationDot, faPhone } from '@fortawesome/free-solid-svg-icons';

import { PersonaliaService } from './personalia.service';
import { LoaderComponent } from '../loader/loader.component';

@Component({
  selector: '[personalia]',
  templateUrl: './personalia.component.html',
  styleUrls: ['./personalia.component.scss'],
  standalone: true,
  imports: [CommonModule, FontAwesomeModule, LoaderComponent],
  providers: [PersonaliaService]
})
export class PersonaliaComponent {

  readonly faEnvelope = faEnvelope;
  readonly faLocation = faLocationDot
  readonly faPhone = faPhone;
  readonly faBirthday = faCakeCandles

  readonly personalia$ = this.service.getPersonalia();

  constructor(private readonly service: PersonaliaService) {}
}
