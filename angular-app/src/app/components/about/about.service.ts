import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { About } from './about.model';
import { simulateApiCall } from 'src/app/util/simulate-api-call.operator';

@Injectable()
export class AboutService {

  constructor(private readonly http: HttpClient) {}

  getAbout(): Observable<About> {
    return this.http.get<About>(`/data/about.json`)
      .pipe(simulateApiCall());
  }
}
