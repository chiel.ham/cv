import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

import { AboutService } from './about.service';
import { LoaderComponent } from '../loader/loader.component';

@Component({
  selector: '[about]',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  standalone: true,
  imports: [CommonModule, LoaderComponent],
  providers: [AboutService]
})
export class AboutComponent {

  readonly about$ = this.service.getAbout();

  constructor(private readonly service: AboutService) {}
}
