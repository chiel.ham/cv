export interface About {
  description: string;
  qualities: string[];
}
