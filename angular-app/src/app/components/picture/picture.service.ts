import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Picture } from './picture.model';
import { simulateApiCall } from 'src/app/util/simulate-api-call.operator';

@Injectable()
export class PictureService {

  constructor(private readonly http: HttpClient) {}

  getPicture(): Observable<Picture> {
    return this.http.get<Picture>(`/data/picture.json`)
      .pipe(simulateApiCall());
  }
}
