import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

import { PictureService } from './picture.service';
import { LoaderComponent } from '../loader/loader.component';

@Component({
  selector: '[picture]',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.scss'],
  standalone: true,
  imports: [CommonModule, LoaderComponent],
  providers: [PictureService]
})
export class PictureComponent {

  readonly picture$ = this.service.getPicture();

  constructor(private readonly service: PictureService) {}
}
