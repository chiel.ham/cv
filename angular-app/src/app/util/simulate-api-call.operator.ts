import { MonoTypeOperatorFunction, delay } from 'rxjs';

/**
 * Returns an operator for simulating the delay of an API call.
 * 
 * The delay is calculated based on some gaussian distribution.
 * 
 * @returns operator that delays the emission of the Observable
 */
export function simulateApiCall<T>(): MonoTypeOperatorFunction<T> {
  // The average of multiple random numbers form a normal distribution
  // By summing two normal distributions, something like a skewed distribution is created
  const delay_ms = 300 * gaussianRand() + 150 * (gaussianRand() + 0.25);
  return delay(delay_ms);
}

const numberOfRandCalls = 8;

function gaussianRand() {
  let i = numberOfRandCalls;
  let randSum = 0;
  while(i--) randSum += Math.random();
  return randSum / numberOfRandCalls;
}